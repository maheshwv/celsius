package sheridan;

import java.util.Scanner;

public class Celsius {
	public static double convertFromFahrenheit(double temp) {
		//double C = Double.parseDouble();
		double F = ((9.0/5.0) * temp) + 32.0;
		return F;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("What is the temperature in Celsius? " + Celsius.convertFromFahrenheit(32) );
		
		double a,c;
 	    Scanner scanner=new Scanner(System.in);	   	 
 	    System.out.println("Enter  Fahrenheit temperature");
 	    a=scanner.nextDouble(); 
 	    System.out.println("Celsius temperature is = "+ Celsius.convertFromFahrenheit(a));
	}
}
