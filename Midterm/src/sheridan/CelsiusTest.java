package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	private void assertCompare(String string) {
		// TODO Auto-generated method stub
		
	}
	@Test
	public void testCelsiusSuccess() {
		assertCompare("Farenheit value = " + Celsius.convertFromFahrenheit(36));
	}
	@Test 
	public void testCelsiusFail() {
		assertCompare("Farenheit value = " + Celsius.convertFromFahrenheit(0));
	}
	@Test 
	public void testBoundryIn() {
		assertCompare("Farenheit value = " + Celsius.convertFromFahrenheit(6));
	}
	@Test 
	public void testBoundryOut() {
		assertCompare("Farenheit value = " + Celsius.convertFromFahrenheit(7));
	}

}
